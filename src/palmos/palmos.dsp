# Microsoft Developer Studio Project File - Name="palmos" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Java Virtual Machine Java Project" 0x0809

CFG=palmos - Java Virtual Machine Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "palmos.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "palmos.mak" CFG="palmos - Java Virtual Machine Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "palmos - Java Virtual Machine Release" (based on\
 "Java Virtual Machine Java Project")
!MESSAGE "palmos - Java Virtual Machine Debug" (based on\
 "Java Virtual Machine Java Project")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
JAVA=jvc.exe

!IF  "$(CFG)" == "palmos - Java Virtual Machine Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "."
# PROP BASE Intermediate_Dir "."
# PROP BASE Target_Dir "."
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ""
# PROP Intermediate_Dir "."
# PROP Target_Dir "."
# ADD BASE JAVA /O
# ADD JAVA /O

!ELSEIF  "$(CFG)" == "palmos - Java Virtual Machine Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "."
# PROP BASE Intermediate_Dir "."
# PROP BASE Target_Dir "."
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ""
# PROP Intermediate_Dir "."
# PROP Target_Dir "."
# ADD BASE JAVA /g
# ADD JAVA /g

!ENDIF 

# Begin Target

# Name "palmos - Java Virtual Machine Release"
# Name "palmos - Java Virtual Machine Debug"
# Begin Group "Source Files"

# PROP Default_Filter "java;html"
# Begin Source File

SOURCE=.\Byte.java
# End Source File
# Begin Source File

SOURCE=.\Date.java
# End Source File
# Begin Source File

SOURCE=.\DateTime.java
# End Source File
# Begin Source File

SOURCE=.\DaySelector.java
# End Source File
# Begin Source File

SOURCE=.\Debug.java
# End Source File
# Begin Source File

SOURCE=.\DmSearchState.java
# End Source File
# Begin Source File

SOURCE=.\Event.java
# End Source File
# Begin Source File

SOURCE=.\FieldAttr.java
# End Source File
# Begin Source File

SOURCE=.\Palm.java
# End Source File
# Begin Source File

SOURCE=.\Pfloat.java
# End Source File
# Begin Source File

SOURCE=.\PointType.java
# End Source File
# Begin Source File

SOURCE=.\Rectangle.java
# End Source File
# Begin Source File

SOURCE=.\SerSettings.java
# End Source File
# Begin Source File

SOURCE=.\SlkPktHeader.java
# End Source File
# Begin Source File

SOURCE=.\SndCommand.java
# End Source File
# Begin Source File

SOURCE=.\SystemPreferences.java
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
