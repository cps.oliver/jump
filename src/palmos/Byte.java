package palmos;

public final class Byte extends Number {
  public Byte(byte v)
  {
    value = v;
  }
  public int intValue()
  {
    return value;
  }
  public long longValue()
  {
    return value;
  }
  public float floatValue()
  {
    return value;
  }
  public double doubleValue()
  {
    return value;
  }
  byte value;
}
